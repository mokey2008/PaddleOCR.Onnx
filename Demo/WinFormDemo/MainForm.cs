﻿
using PaddleOCR.Onnx;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
namespace PaddleOCR.Onnx.Demo
{
    /// <summary>
    /// PaddleOCRSharp使用示例
    /// </summary>
    public partial class MainForm : Form
    {
        private PaddleOCREngine engine;
        Bitmap bmp;
        public MainForm()
        {
            InitializeComponent();


            //自带轻量版中英文模型V3
            OCRModelConfig config = null;

           
            //英文数字模型,自行在QQ群下载
            //OCRModelConfig config = new OCRModelConfig();
            //string root = Environment.CurrentDirectory;
            //string modelPathroot = root + @"\en";
            //config.det_infer = modelPathroot + @"\ch_PP-OCRv2_det_infer.onnx";
            //config.cls_infer = modelPathroot + @"\ch_ppocr_mobile_v2.0_cls_infer.onnx";
            //config.rec_infer = modelPathroot + @"\en_number_mobile_v2.0_rec_infer.onnx";
            //config.keys = modelPathroot + @"\en_dict.txt";

            //OCR参数
            OCRParameter oCRParameter = new OCRParameter();
            oCRParameter.MaxSideLen = 960; 
            oCRParameter.numThread = 6;//预测并发线程数
            oCRParameter.Enable_mkldnn = true;//web部署该值建议设置为0,否则出错，内存如果使用很大，建议该值也设置为0.

            //初始化OCR引擎
            engine = new PaddleOCREngine(config, oCRParameter);

        }
       
        /// <summary>
        /// 打开本地图片
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripopenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "*.*|*.bmp;*.jpg;*.jpeg;*.tiff;*.tiff;*.png";
            if (ofd.ShowDialog() != DialogResult.OK) return;
            var imagebyte = File.ReadAllBytes(ofd.FileName);
            bmp = new Bitmap(new MemoryStream(imagebyte));
            pictureBox1.BackgroundImage = bmp;
            richTextBox1.Clear();
            if (bmp == null) return;
            OCRResult ocrResult = engine.DetectText(imagebyte);
            ShowOCRResult(ocrResult);
        }
      
        /// <summary>
        /// 识别截图文本
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripLabel2_Click(object sender, EventArgs e)
        {
            this.Hide();
            richTextBox1.Clear();
            System.Threading.Thread.Sleep(200);

            ScreenCapturer.ScreenCapturerTool screenCapturer = new ScreenCapturer.ScreenCapturerTool();
            if (screenCapturer.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                bmp = (Bitmap)screenCapturer.Image;
                pictureBox1.BackgroundImage = bmp;
                try
                {
                   ;
                    OCRResult ocrResult  = engine.DetectText(bmp);
                   ShowOCRResult(ocrResult);
                    
                }
                catch (Exception ex)
                {
                }

            }
            this.Show();
        }
       


        /// <summary>
        /// 显示结果
        /// </summary>
        private void ShowOCRResult(OCRResult ocrResult)
        {
            Bitmap bitmap = (Bitmap)this.pictureBox1.BackgroundImage;
            foreach (var item in ocrResult.TextBlocks)
            {
               richTextBox1.AppendText(item.Text + "\n");
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.DrawPolygon(new Pen(Brushes.Red,2), item.BoxPoints.Select(x=>new PointF() { X=x.X,Y=x.Y}).ToArray());
                }
            }
            pictureBox1.BackgroundImage = null;
            pictureBox1.BackgroundImage = bitmap;
        }

      

        private void MainForm_Load(object sender, EventArgs e)
        {

        }
    }
}
